package cn.likecy;

import cn.likecy.admin.service.FeilListService;
import cn.likecy.service.HttpAPIService;
import com.alibaba.fastjson.JSONObject;
import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeGenWrapper;
import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeOptions;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilePreviewApplicationTests {

	@Autowired
	private HttpAPIService httpAPIService;

	@Test
	public void testGenWxQrcode() {
	    try {
            String str = httpAPIService.doGet("https://api.ooopn.com/image/sogou/api.php?type=json");
//            System.out.println(str["imgurl"]);
            JSONObject jsStr = JSONObject.parseObject(str);
            System.out.println(jsStr.getString("imgurl"));
        }catch (Exception e){

        }
	}
}
