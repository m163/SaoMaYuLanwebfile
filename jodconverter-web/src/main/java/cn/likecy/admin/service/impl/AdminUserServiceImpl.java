package cn.likecy.admin.service.impl;

import cn.likecy.admin.model.AdminUser;
import cn.likecy.admin.mapper.AdminUserMapper;
import cn.likecy.admin.service.AdminUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
@Service
public class AdminUserServiceImpl extends ServiceImpl<AdminUserMapper, AdminUser> implements AdminUserService {


    public  boolean login(String username, String password){
        List<AdminUser> users =getBaseMapper().selectList(null);
        for (AdminUser u :users ){
            if (u.getUsername().equals(username)&&u.getPasswd().equals(password)){
                return true;
            }
        }
          return  false;
    }


}
