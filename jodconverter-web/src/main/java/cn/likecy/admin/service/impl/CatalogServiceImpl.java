package cn.likecy.admin.service.impl;

import cn.likecy.admin.model.Catalog;
import cn.likecy.admin.mapper.CatalogMapper;
import cn.likecy.admin.service.CatalogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
@Service
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, Catalog> implements CatalogService {

}
