package cn.likecy.admin.service;

import cn.likecy.admin.model.AdminUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
public interface AdminUserService extends IService<AdminUser> {

}
