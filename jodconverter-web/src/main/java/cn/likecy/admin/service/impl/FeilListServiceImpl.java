package cn.likecy.admin.service.impl;

import cn.likecy.admin.model.FeilList;
import cn.likecy.admin.mapper.FeilListMapper;
import cn.likecy.admin.service.FeilListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
@Service
public class FeilListServiceImpl extends ServiceImpl<FeilListMapper, FeilList> implements FeilListService {

}
