package cn.likecy.admin.web;


import cn.likecy.admin.service.impl.AdminUserServiceImpl;
import cn.likecy.config.ConfigConstants;
import cn.likecy.utils.DrawingUtils;
import cn.likecy.utils.Result;
import cn.likecy.utils.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
//@Controller
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminUserController {

    @Autowired
    private AdminUserServiceImpl adminUserService;

    @RequestMapping(value = "/login")
    public Result login(@RequestParam("username") String username, @RequestParam("password") String passwd){

      if (adminUserService.login(username,passwd)){
          return ResultGenerator.genSuccessResult("登录成功");
      }
        return  ResultGenerator.genFailResult("登录失败");
    }


    @RequestMapping("/getcode")
    public Result getCode(String url,@RequestParam(value ="name",defaultValue ="") String text,HttpServletRequest request){
        String severip= getServerIPPort(request);
        String codeurl = ConfigConstants.getFileDir()+"qcode/";
        File file1 = new File(codeurl);
        String outUrl = severip+"/onlinePreview?url="+url;
        if(!file1.exists()){
            file1.mkdirs();
        }
        String qcodename = UUID.randomUUID().toString()+".png";
        String logo = severip+"/images/run22.gif";
        try {
            DateFormat data= DateFormat.getDateInstance();
            DrawingUtils.encoderQRCode(outUrl,logo,codeurl+qcodename,"png",1000,text+"--"+data.format(new Date()));
        } catch (Exception e) {
            return  ResultGenerator.genFailResult(e.getMessage());
        }
        return ResultGenerator.genSuccessResult(severip+"/qcode/"+qcodename);
    }



    /**
     * 获取服务部署根路径 http:// + ip + port
     *
     * @param request
     * @return
     */
    private String getServerIPPort(HttpServletRequest request) {
        //+ ":" + request.getServerPort()
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }



}

