package cn.likecy.admin.web;


import cn.likecy.admin.mapper.CatalogMapper;
import cn.likecy.admin.model.Catalog;
import cn.likecy.admin.service.CatalogService;
import cn.likecy.admin.service.FeilListService;
import cn.likecy.config.ConfigConstants;
import cn.likecy.service.HttpAPIService;
import cn.likecy.utils.Result;
import cn.likecy.utils.ResultGenerator;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
@RestController
//@Controller
@CrossOrigin
@RequestMapping("/catalog")
public class CatalogController {

    @Autowired
    private CatalogMapper catalogMapper;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private FeilListService feilListService;
    @Autowired
    private HttpAPIService httpAPIService;


    /**
     * 添加分类目录
     *
     * @param catalog {    *name
     *                * image
     *                * dir
     *                }
     * @return SUCCESS
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(String name, String dir) {
        Catalog catalog = new Catalog();
        catalog.setName(name);
        catalog.setDir(dir);
        try {
            String res = httpAPIService.doGet("https://api.ooopn.com/image/sogou/api.php?type=json");
            JSONObject jsStr = JSONObject.parseObject(res);
            String imageUrl = jsStr.getString("imgurl");
            catalog.setImage(imageUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (catalogMapper.insert(catalog) == 1) {
            String uploadDir = ConfigConstants.getFileUploadDir();
            String delDir = uploadDir + dir;
            File delectDir = new File(delDir);
            if (!delectDir.exists()) delectDir.mkdirs();
            return ResultGenerator.genSuccessResult(catalog);
        } else {
            return ResultGenerator.genFailResult("添加失败");
        }
    }


    /**
     * 图片句子
     *
     * @return
     */
    @RequestMapping("/getnews")
    public Result getNews() {
        Map<String, Object> newData = new HashMap<String, Object>();
        try {
            String txt = httpAPIService.doGet("https://api.xygeng.cn/dailywd/api/api.php");
            String img = httpAPIService.doGet("https://api.ooopn.com/image/sogou/api.php?type=json");
            JSONObject jsStr = JSONObject.parseObject(img);
            String imageUrl = jsStr.getString("imgurl");
            newData.put("img", imageUrl);
            newData.put("txt", txt);
        } catch (Exception e) {
        }
        return ResultGenerator.genSuccessResult(newData);
    }


    /**
     * 删除分类
     *
     * @param id DELETE
     * @return
     */

    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delete(@RequestParam("id") Integer id) {
        Map<String, Object> finder = new HashMap<String, Object>();
        finder.put("categore_id", id);
        String dirName = catalogService.getById(id).getDir();
        boolean tableRes = feilListService.removeByMap(finder);
        String uploadDir = ConfigConstants.getFileUploadDir();
        String delDir = uploadDir + dirName;
//        System.out.println("======================");
//        System.out.println(delDir);
//        System.out.println("======================");
        if (tableRes) {
            if (delectFolder(delDir)) {
                if (catalogMapper.deleteById(id) == 1) {
                    return ResultGenerator.genSuccessResult("删除成功");
                } else return ResultGenerator.genFailResult("删除失败");
            } else return ResultGenerator.genFailResult("递归删除失败");
        } else return ResultGenerator.genFailResult("文件列表删除失败");

    }

    /**
     * 获取分类列表
     *
     * @return data
     */
    @RequestMapping("/list")
    public Result list() {
        return ResultGenerator.genSuccessResult(catalogMapper.selectList(null));
    }


    /**
     * 修改目录信息
     *
     * @param catalog {
     *                id
     *                name
     *                image
     *                dir
     *                }
     * @return SUCCESS
     */
    @RequestMapping(value = "/change", method = RequestMethod.POST)
    public Result changeDir(String name, Integer id, boolean updataimg) {
        Catalog catalog = new Catalog();
        catalog.setName(name);
        catalog.setId(id);
        if (updataimg) {
            try {
                String res = httpAPIService.doGet("https://api.ooopn.com/image/sogou/api.php?type=json");
                JSONObject jsStr = JSONObject.parseObject(res);
                String imageUrl = jsStr.getString("imgurl");
                catalog.setImage(imageUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResultGenerator.genSuccessResult(catalogMapper.updateById(catalog));
    }


    /**
     * 获取目录详细信息
     *
     * @param id
     * @return
     */
    @RequestMapping("/get/{id}")
    public Result getById(@PathVariable("id") Integer id) {
        Catalog res = catalogService.getById(id);
        if (res != null)
            return ResultGenerator.genSuccessResult(res);
        else return ResultGenerator.genFailResult("目录不存在");
    }


    /**
     * 删除目录及其子文件
     * @param url
     * @return
     */
    private boolean delectFolder(String url) {
        File file = new File(url);
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            file.delete();
            return true;
        } else {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                String root = files[i].getAbsolutePath();
                delectFolder(root);
            }
            file.delete();
            return true;
        }
    }

}

