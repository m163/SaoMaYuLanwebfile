package cn.likecy.admin.web;


import cn.likecy.admin.model.Catalog;
import cn.likecy.admin.model.FeilList;
import cn.likecy.admin.service.CatalogService;
import cn.likecy.admin.service.FeilListService;
import cn.likecy.config.ConfigConstants;
import cn.likecy.utils.Result;
import cn.likecy.utils.ResultGenerator;
import cn.likecy.utils.ServiceException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
@RestController
@CrossOrigin
@RequestMapping("/file")
public class FeilListController {

    @Autowired
    private FeilListService feilListService;
    @Autowired
    private CatalogService catalogService;
//    private ConfigConstants configConstants;


    /**
     * 上传文件
     * @param file
     * @param catalog 分类id
     * @param request
     * @return 成功or失败 SUCCESS
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/upload")
    public Result uploadFeil(MultipartFile file, int catalog , HttpServletRequest request) throws JsonProcessingException {
        String serverIPPort = getServerIPPort(request);
        String fileLocalurl;
        String fileName =file.getOriginalFilename();
        String uploadDir = ConfigConstants.getFileUploadDir();
        String outPutUrl ="/upload/";
        if (catalog!=0) {
           String catalogName = getCatalogNameById(catalog);
            outPutUrl+=catalogName;
            outPutUrl+="/";
            uploadDir += catalogName;
            uploadDir += "/";
        }
        File file1 = new File(uploadDir);
        if (!file1.exists()) {
            file1.mkdirs();
        }
        try {
            fileLocalurl = uploadDir +fileName;
            File file2 = new File(fileLocalurl);
            if(!file2.exists()){
                file.transferTo(file2);
                String backUrl = getBackUrl(serverIPPort,outPutUrl+fileName);
                FeilList uploader = new FeilList();
                uploader.setUrl(backUrl);
                uploader.setName(fileName);
                uploader.setDelateUrl(fileLocalurl);
                uploader.setDate(LocalDateTime.now());
                uploader.setCategoreId(catalog);
                try {
                    feilListService.save(uploader);
                }catch (Exception e){
                    file2.delete();
                    throw new ServiceException("文件保存失败请重新上传");
                }
            }else {
                return ResultGenerator.genFailResult("文件已存在请检查分类目录");
            }
            //保存数据库若失败则删除文件返回上传失败
        } catch (Exception e) {
            throw new ServiceException("文件保存失败请重新上传");
        }
        return ResultGenerator.genSuccessResult();
    }

    /**
     * 获取文件列表
     * @return data数组
     */
    @RequestMapping("/file_list")
    public Result list(){
       return ResultGenerator.genSuccessResult(feilListService.list(null));
    }



    /**
     * 通过分类获取文件列表
     * @param catalog
     * @return
     */
    @RequestMapping("/getbycatalog")
    public Result getByCatalogId(@RequestParam("catalog")Integer catalog){
        Map<String,Object> finder = new HashMap<String, Object> ();
        finder.put("categore_id",catalog);
       Collection<FeilList> res= feilListService.listByMap(finder);
       if(res.size()!=0){
           return ResultGenerator.genSuccessResult(res);
       }else {
           return ResultGenerator.genFailResult("数据为空");
       }

    }

    /**
     * 通过分类id获取分类文件路径
     * @param id
     * @return
     */
    private String getCatalogNameById(int id){
       Catalog catalog = catalogService.getById(id);
       return  catalog.getDir();
    }


    /**
     * 修改文件显示名称
     * @param id
     * @param name
     * @return
     */
    @RequestMapping("/changename")
    private  Result changeName(@RequestParam("id") Integer id,@RequestParam("name") String name){
        FeilList changeid = new FeilList();
        changeid.setId(id);
        changeid.setName(name);
        if(feilListService.updateById(changeid)){
           return  ResultGenerator.genSuccessResult();
        }else {
            return  ResultGenerator.genFailResult("更新失败");
        }

    }

    /**
     * 上传图片
     *
     * @param  image
     * @return data：url
     */
    @RequestMapping("/upload_img")
    public Result uploadImg(@RequestParam("image") MultipartFile img ,HttpServletRequest request){
       try {
           BufferedImage bi = ImageIO.read(img.getInputStream());
           if (bi == null){
               throw  new ServiceException("上传的文件不是图片");
           }
       }catch (Exception e){
           return  ResultGenerator.genFailResult(e.getMessage());
       }
        String uploadDir = ConfigConstants.getFileUploadDir()+"/img/";
        File file1 = new File(uploadDir);
        if (!file1.exists()) {
            file1.mkdirs();
        }
        File file2 = new File(uploadDir+img.getOriginalFilename());
        String serverIPPort = getServerIPPort(request);
            try{
                img.transferTo(file2);
            }catch (Exception e){
                throw  new ServiceException(e.getMessage());
            }
            String backUrl = getBackUrl(serverIPPort,"/upload/img/"+img.getOriginalFilename());
            return ResultGenerator.genSuccessResult(backUrl);
    }

    /**
     * 删除文件
     * @param dir
     * @return
     */

    @RequestMapping(value ="/del",method = RequestMethod.POST)
    public  Result delectFile(@RequestParam(value = "dir" ,required = true) String dir){
        Map<String,Object> del = new HashMap<String, Object>();
        del.put("delate_url",dir);
        File delfile =new File(dir);
        if (delfile.exists()){
            boolean delStatus = feilListService.removeByMap(del);
            if (delStatus) {
                delfile.delete();
                return ResultGenerator.genSuccessResult("删除成功");
            }else  return  ResultGenerator.genFailResult("数据库失败");
        }else {
            return  ResultGenerator.genFailResult("文件不存在了");
        }
    }






    /**
     * 获取服务部署根路径 http:// + ip + port
     *
     * @param request
     * @return
     */
    private String getServerIPPort(HttpServletRequest request) {
        //+ ":" + request.getServerPort()
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }


    /**
     * 获取文件回调url
     *
     * @param serverName
     *            服务路径
     * @param fileName
     *            文件名
     * @return 回调url
     */
    private String getBackUrl(String serverName, String fileName) {
        StringBuffer backUrl = new StringBuffer();
        backUrl.append(serverName).append(fileName);
        return backUrl.toString();
    }


    /**
     * 获取统计数据
     * @return
     */
    @RequestMapping("/gettotal")
    public  Result getCatalogStatistics(){
      List<Catalog>  lists =  catalogService.list();
//      Integer catalogCount = lists.size();

      JSONArray findfinder = new JSONArray();
//      StringArray nameData =new StringArray();
        List<String> nameData=new ArrayList<String>();
      for(Catalog catalog : lists){
          String name =catalog.getName();
          Map<String,Object> totals= new HashMap<String, Object>();
          Map<String,Object> finder = new HashMap<String, Object>();
          Integer catalogId = catalog.getId();
        finder.put("categore_id",catalogId);
         Collection<FeilList> feilLists = feilListService.listByMap(finder);
         Integer feilCount = feilLists.size();
         totals.put("name",name);
         totals.put("value",feilCount);
         JSONObject json = new JSONObject(totals);
         findfinder.add(json);
          nameData.add(name);
      }
        Map<String,Object> show= new HashMap<String, Object>();
      show.put("nameData",nameData);
      show.put("showData",findfinder);

        return ResultGenerator.genSuccessResult(show);
    }
}

