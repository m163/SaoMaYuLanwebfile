package cn.likecy.admin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
public class AdminUser implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @NotEmpty(message ="用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message ="密码锁不能为空")
    @Length(min =6,message = "密码长度至少为6位")
    private String passwd;

    /**
     * 手机号
     */
    private String tel;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "AdminUser{" +
        "id=" + id +
        ", username=" + username +
        ", passwd=" + passwd +
        ", tel=" + tel +
        "}";
    }
}
