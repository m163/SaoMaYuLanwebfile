package cn.likecy.admin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author chenyun
 * @since 2019-06-09
 */
public class FeilList implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分类名
     */
    private Integer categoreId;

    /**
     * 名称
     */
    private String name;

    /**
     * 文件链接
     */
    private String url;

    /**
     * 文件删除路径
     */
    private String delateUrl;

    /**
     * 上传时间
     */
    private LocalDateTime date;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoreId() {
        return categoreId;
    }

    public void setCategoreId(Integer categoreId) {
        this.categoreId = categoreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDelateUrl() {
        return delateUrl;
    }

    public void setDelateUrl(String delateUrl) {
        this.delateUrl = delateUrl;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FeilList{" +
        "id=" + id +
        ", categoreId=" + categoreId +
        ", name=" + name +
        ", url=" + url +
        ", delateUrl=" + delateUrl +
        ", date=" + date +
        "}";
    }
}
