package cn.likecy.admin.mapper;

import cn.likecy.admin.model.Catalog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenyun
 * @since 2019-06-05
 */
public interface CatalogMapper extends BaseMapper<Catalog> {

}
