package cn.likecy.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io .*;

import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeGenWrapper;
import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeOptions;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
/**
 * @auther: chenyun
 * @time: 2019-06-07
 * @description Java 图片生成
 */
public class DrawingUtils {



    // 二维码宽度
    private final static int QRCODE_WIDTH = 1000;
    // 二维码高度
    private final static int QRCODE_HEIGHT = 1000;

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param imgPath 图片路径
     * @param imgType 图片类型
     * @param text    底部汉字
     */
    public static void encoderQRCode(String content, String logo, String imgPath, String imgType, String text) {
        encoderQRCode(content, logo,imgPath, imgType, 2, text);
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param imgPath 图片路径
     * @param imgType 图片类型
     * @param text    底部汉字
     * @param size    二维码尺寸
     */
    public static void encoderQRCode(String content,  String logo,String imgPath, String imgType, int size, String text) {

        try {
            //创建一个二维码
//            BufferedImage bufImg = qRCodeCommon(content, imgType, size);
            BufferedImage  bufImg = QrCodeGenWrapper.of(content)
                    .setW(size)
                    .setH(size)
//                    .setDetectImg("detect.png")
                    .setDrawPreColor(0xff008e59)
//                    .setDrawPreColor(0xff002fa7)
                    .setErrorCorrection(ErrorCorrectionLevel.M)
                    .setLogoStyle(QrCodeOptions.LogoStyle.ROUND)
                    .setLogoBgColor(Color.LIGHT_GRAY)
                    .setLogo(logo)
                    .setLogoRate(10)
                    .setDrawStyle(QrCodeOptions.DrawStyle.CIRCLE)
                    .setDrawEnableScale(true)
                    .asBufferedImage();
            bufImg = (BufferedImage) flexible(bufImg, QRCODE_WIDTH, QRCODE_HEIGHT);//调整二维码的大小
            bufImg = addNote(bufImg, text);//添加底部文字
            File imgFile = new File(imgPath);
            //判断路径是否存在，否：创建此路径 根据需要自己判断
            if (!imgFile.getParentFile().getParentFile().getParentFile().exists()) {//判断
                imgFile.getParentFile().getParentFile().getParentFile().mkdirs();
            }
            if (!imgFile.getParentFile().getParentFile().exists()) {
                imgFile.getParentFile().getParentFile().mkdirs();
            }
            if (!imgFile.getParentFile().exists()) {
                imgFile.getParentFile().mkdirs();
            }
            imgFile.createNewFile();
            // 生成二维码QRCode图片
            ImageIO.write(bufImg, imgType, imgFile);
        } catch (Exception e) {
            e.printStackTrace();
              throw new  ServiceException(e.getMessage());
        }
    }

    /**
     * BufferedImage的伸缩
     *
     * @return
     */
    public static Image flexible(Image image, int width, int height) {
        // 获取一个自定义宽高的图像实例
        Image src = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        //缩放图像
        BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = tag.createGraphics();
        g.drawImage(src, 0, 0, null);// 绘制缩小后的图
        g.dispose();
        return tag;

    }

    /**
     * 给二维码下方添加说明文字
     *
     * @param image原二维码
     * @param note说明文字
     * @return 带说明文字的二维码
     */
    public static BufferedImage addNote(BufferedImage image, String note) {
        Image src = image.getScaledInstance(1000, 1000, Image.SCALE_DEFAULT);
        BufferedImage tag;
        if (note.length() <= 16) {
            tag = new BufferedImage(1000, 1044, BufferedImage.TYPE_INT_RGB);
        } else {
            tag = new BufferedImage(1000, 1088, BufferedImage.TYPE_INT_RGB);
        }
        Graphics g1 = tag.getGraphics();//设置低栏白边
        Graphics2D g2 = tag.createGraphics();//设置文字
        Font font = new Font("宋体", Font.ITALIC, 24);
        g2.setFont(font);
        g2.setColor(Color.BLACK);
        if (note.length() <= 16) {
            g1.fillRect(0, QRCODE_HEIGHT, QRCODE_WIDTH, 44);
            g2.drawString(note, QRCODE_WIDTH / 2 - note.length() * 8 - 14, QRCODE_HEIGHT + font.getSize());
        } else {
            g1.fillRect(0, QRCODE_HEIGHT, QRCODE_WIDTH, 88);
//            g2.drawString(note.substring(0, 16), 5, QRCODE_HEIGHT + font.getSize());
            g2.drawString(note, QRCODE_WIDTH / 2 - (note.length()) * 8 - 14, QRCODE_HEIGHT + font.getSize() * 2 + 4);
        }
        g1.drawImage(src, 0, 0, null);
        g1.dispose();
        g2.dispose();
        image = tag;
        image.flush();
        return image;
    }

}
