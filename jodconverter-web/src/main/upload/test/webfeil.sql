/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : webfeil

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 07/06/2019 16:10:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `passwd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `tel` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
BEGIN;
INSERT INTO `admin_user` VALUES (1, 'chenyun', 'chenyun666', '18715706435');
INSERT INTO `admin_user` VALUES (2, 'cuixiang', 'cuixiang666', '110');
COMMIT;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '分类目录名',
  `dir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '路径英文名',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件夹图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of catalog
-- ----------------------------
BEGIN;
INSERT INTO `catalog` VALUES (13, '测试2', 'test', NULL);
INSERT INTO `catalog` VALUES (15, '崔向', 'cuixiang', 'http://likecy.cn');
COMMIT;

-- ----------------------------
-- Table structure for feil_list
-- ----------------------------
DROP TABLE IF EXISTS `feil_list`;
CREATE TABLE `feil_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categore_id` int(3) DEFAULT '0' COMMENT '分类号',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件链接',
  `delate_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件删除路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of feil_list
-- ----------------------------
BEGIN;
INSERT INTO `feil_list` VALUES (1, 0, '233', '111', '111');
INSERT INTO `feil_list` VALUES (2, 0, '提拉米苏表白墙源码.zip', 'http://localhost:8080/upload/提拉米苏表白墙源码.zip', '/Users/chenyun/desktop/test/upload/提拉米苏表白墙源码.zip');
INSERT INTO `feil_list` VALUES (3, 0, '格式化工具.zip', 'http://localhost:8080/upload/格式化工具.zip', '/Users/chenyun/desktop/test/upload/格式化工具.zip');
INSERT INTO `feil_list` VALUES (4, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (5, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (6, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (7, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (8, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (9, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (10, 0, 'v1.0.2.zip', 'http://localhost:8080/upload/v1.0.2.zip', '/Users/chenyun/desktop/test/upload/v1.0.2.zip');
INSERT INTO `feil_list` VALUES (11, 0, '屏幕录制 2019-06-02 下午8.25.00.mov', 'http://localhost:8080/upload/javademo/屏幕录制 2019-06-02 下午8.25.00.mov', '/Users/chenyun/desktop/test/upload/javademo/屏幕录制 2019-06-02 下午8.25.00.mov');
INSERT INTO `feil_list` VALUES (12, 0, 'bbs_love.sql', 'http://localhost:8080/upload/javademo/bbs_love.sql', '/Users/chenyun/desktop/test/upload/javademo/bbs_love.sql');
INSERT INTO `feil_list` VALUES (13, 0, 'latest.zip', 'http://localhost:8080/upload/javademo/latest.zip', '/Users/chenyun/desktop/test/upload/javademo/latest.zip');
INSERT INTO `feil_list` VALUES (14, 1, '2014-C-B题10生命之树-3.mp4', 'http://localhost:8080/upload/cuixiang/2014-C-B题10生命之树-3.mp4', '/Users/chenyun/desktop/test/upload/cuixiang/2014-C-B题10生命之树-3.mp4');
INSERT INTO `feil_list` VALUES (15, 1, '2PODhmrvLik.jpg', 'http://localhost:8080/upload/cuixiang/2PODhmrvLik.jpg', '/Users/chenyun/desktop/test/upload/cuixiang/2PODhmrvLik.jpg');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
