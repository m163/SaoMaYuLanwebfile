/*
 Navicat Premium Data Transfer

 Source Server         : 10.11.2.17测试服账号
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : 10.11.2.17:3306
 Source Schema         : webfeil

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 11/06/2019 06:20:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `passwd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `tel` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '分类目录名',
  `dir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '路径英文名',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '自动生成图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for feil_list
-- ----------------------------
DROP TABLE IF EXISTS `feil_list`;
CREATE TABLE `feil_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categore_id` int(10) DEFAULT '0' COMMENT '分类名',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件链接',
  `delate_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件删除路径',
  `date` datetime DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
