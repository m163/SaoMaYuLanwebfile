# 扫码预览webfile

#### 介绍
这个是最近的java作业，在线文件预览，老师后台上传文件，导出二维码供上课使用。
后台可以上传文件，删除文件，分类目录，统计数据实时报表，修改文件，导出二维码，随机小句子。

#### 软件架构
软件架构说明
springboot2, mybatis-plus, vue,FreeMarker模版引擎，elementelement UI，echarts，openoffice，rad缓存，定时任务，maven

#### 安装教程

1. git clone https://gitee.com/likecy/SaoMaYuLanwebfile.git 导入ij
2. 开发环境的话，请自行安装openoffice软件，http://www.openoffice.org
3.等待maven插件安装完成后去conf目录配置好数据库文件，启动

#### 使用说明

1. 本仓库基于苹果系统开发 ，跑linux服务器bug未知

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
